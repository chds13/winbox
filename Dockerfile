FROM ubuntu

MAINTAINER Danila Chepurko <chds@tuta.io>

RUN dpkg --add-architecture i386 &&\
    apt-get -q update &&\
    apt-get install --no-install-recommends --assume-yes wine32 &&\
    apt-get clean &&\
    useradd -m winer

ADD https://mt.lv/winbox /home/winer/winbox.exe
RUN /bin/mkdir -p /home/winer/.wine/drive_c/users/winer/Application\ Data/Mikrotik/Winbox &&\
    /bin/chown -R winer: /home/winer &&\
    /bin/chmod 755 /home/winer/winbox.exe
USER winer

ENTRYPOINT /usr/lib/wine/wineserver32 &&\
           /usr/lib/wine/wine /home/winer/winbox.exe
